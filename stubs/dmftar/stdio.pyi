PROG: str
QUIET: bool
DEBUG: bool
DEBUG_COLORS: bool

def stdout(msg: str) -> None: ...
def stderr(msg: str) -> None: ...
def warning(msg: str) -> None: ...
def error(msg: str) -> None: ...
def debug(msg: str) -> None: ...
