import argparse
from dmftar.config import Config
from typing import List, Optional

PROG: str
VERSION: str

class OptionError(Exception): ...

class Args:
    tar_subcommand: Optional[str]
    remote: Optional[str]
    archive_dir: Optional[str]
    patterns: List[str]
    force_local: bool
    opt_null: bool
    regen_index: bool
    regen_checksum: bool
    def __init__(self) -> None: ...
    def get_options(self, conf: Config) -> None: ...
    def check_args(self, args: List[str], parser: argparse.ArgumentParser) -> None: ...

def require_gnu_tar(tar_program: str) -> None: ...
def scoped_main() -> None: ...
def run() -> None: ...
