from typing import Set, Optional

DMF_CAPABLE: Optional[Set[int]]

def dmf_capable(path: str) -> bool: ...
