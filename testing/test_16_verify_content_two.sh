#! /bin/bash
#
#	test_verify_content_two.sh
#

if [ ! -d "${DATASET}.dmftar" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar"
	exit 1
fi


test_content() {
	# $1 is tar volume file
	# Returns 1 on error

	# list what is in the tar volume and see if it's in the directory
	# we check file name and size

	local entry=0

	$TAR tvf "$1" 2>/dev/null |awk '{ print $3 " " $NF }' |while read T_SIZE T_FILE
	do
#		echo "TD testing  $T_SIZE  $T_FILE"

		entry=$((entry + 1))

		if [ "${T_FILE: -1}" == "/" ]
		then
			# it's a directory
			if [ ! -d "$T_FILE" ]
			then
				echo "fail: no such directory: $T_FILE"
				return 1
			fi
			continue
		fi

		# detect end of multi-volume tar file
		END=$(echo "$T_FILE" |sed 's/^[0-9]\+--$/match/')
		if [ "$END" == "match" ]
		then
#			echo "TD end of volume reached"
			continue
		fi

		if [ $entry -eq 1 ]
		then
			if [[ $T_FILE =~ .*/GNUFileParts\.[[:digit:]]+/.* || $T_FILE =~ .*/GNUFileParts/.* ]]
			then
#				echo "TD continued multi-volume file: $T_FILE"
				continue
			fi
		fi

		if [ ! -e "$T_FILE" ]
		then
			echo "fail: no such file: $T_FILE"
			return 1
		fi

#		ORIG_SIZE=$(stat --printf="%s" $T_FILE)
		ORIG_SIZE=$($FILESIZE $T_FILE)
		if [ $ORIG_SIZE -ne $T_SIZE ]
		then
			echo "fail: size mismatch for $T_FILE"
			return 1
		fi
	done
}


for volume in ${DATASET}.dmftar/0000/*.tar*
do
	test_content $volume
	if [ $? -ne 0 ]
	then
		exit 1
	fi
done


# also test other way around;
# see what's in the original dataset dir and
# check that it's present in the tar volumes

TEST_INDEX="$TMPDIR/test_index"
# create/truncate index file
> "$TEST_INDEX"

for volume in ${DATASET}.dmftar/0000/*.tar*
do
	$TAR tf $volume 2>/dev/null >>"$TEST_INDEX"
done

test_against_index() {
	# check that all files under dataset are in the index
	# Returns 1 on error

	find "$DATASET" -type f -print |while read FILENAME
	do
		egrep -q "^${FILENAME}\$" "$TEST_INDEX"
		if [ $? -ne 0 ]
		then
			echo "fail: missing from index: $FILENAME"
			return 1
		fi
	done
}

test_against_index
exit $?

# EOB
