#! /bin/bash
#
#	runtests.sh		WJ118
#	Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

#PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
#export PATH

# some terminal settings (like xterm-256color) will insert weird trash
# in the output, causing tests to fail
unset TERM

LIBDIR=$(dirname $0)
if [ -z "$LIBDIR" ]
then
	LIBDIR="."
fi
export LIBDIR

USER=${USER:-root}
export USER
TMPDIR=/tmp/dmftar-testing.$USER
export TMPDIR
mkdir -p -m 750 $TMPDIR

# set the dataset to use
DATASET=${DATASET:-dataset1}
if [ ! -d "$DATASET" ]
then
	echo "error: no such dataset directory: $DATASET"
	exit 255
fi
export DATASET

# set the 'dmftar' command to run
# NOTE: $DMFTAR can not be a relative path like "../dmftar" ;
# use a command that is on PATH or else an abspath
# (Some tests do chdir and then the test breaks)
# NOTE: $DMFTAR must be a script that also sets PYTHONPATH
# NOTE: $DMFTAR runs dmftar-volume-changer ... beware it chooses the correct one
# NOTE: if the alternate $DMFTAR script passes any extra options, test_01_usage will always fail
DMFTAR=${DMFTAR:-dmftar}
DMFTAR="$DMFTAR --debug"	# turn on debugging
export DMFTAR

# quick n dirty way of doing BSD compatibility
if [[ $(uname) == "Linux" ]]
then
	TAR=tar
	FILESIZE="stat --printf=%s"
else
	TAR=gtar
	FILESIZE="stat -f %z"
fi
export TAR FILESIZE


split_debug_output() {
	# split output into ouput.debug and normal output
	# $1 is filename
	grep ^% "$1" >"${1}.debug"
	grep -v ^% "$1" >"${1}.tmp"
	mv "${1}.tmp" "$1"
}
# export this function to subshells
export -f split_debug_output


echo "---------------------------------------------------------------"
echo " dmftar-testing"
echo "---------------------------------------------------------------"

# run all the tests
passed=0
failed=0
n=1

for test in $LIBDIR/test*.sh
do
	testname=$(basename $test)
	printf "\x1b[33;1m[$n]\x1b[0m running $testname\n"

	fname=$(basename $test)
	fname=$(echo $fname |sed 's/\.sh$//')
	OUTPUT="$TMPDIR/${fname}.out"
	export OUTPUT

	if [ -x $test ]
	then
		$test
		if [ $? -eq 0 ]
		then
			# green
			printf "\x1b[32;1m[$n] OK\x1b[0m      $testname\n"
			passed=$(($passed + 1))
		else
			# red
			printf "\x1b[31;1m[$n] FAIL\x1b[0m    $testname\n"
			failed=$(($failed + 1))
		fi
	else
		# yellow
		printf "\x1b[33;1m[$n] SKIP\x1b[0m    $testname\n"
	fi
	n=$((n + 1))
done

echo  "---------------------------------------------------------------"

if [ $failed -eq 0 ]
then
	# green
	printf "\x1b[32;1mSUCCESS\x1b[0m      $passed passed, $failed failed\n\n"
	exit 0
else
	# red
	printf "\x1b[31;1mFAIL\x1b[0m         $passed passed, $failed failed\n\n"
	exit 1
fi

# EOB
