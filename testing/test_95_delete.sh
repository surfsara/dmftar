#! /bin/bash
#
#	test_delete.sh
#

if [ ! -d "${DATASET}.dmftar" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar"
	exit 1
fi

$DMFTAR --delete-archive -f "${DATASET}.dmftar" >$OUTPUT 2>&1
RET=$?

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -eq 0 ]
then
	echo "fail: there were errors"
	exit 1
fi

if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

if [ -d "${DATASET}.dmftar" ]
then
	echo "fail: directory ${DATASET}.dmftar still exists"
	exit 1
fi

# EOB
