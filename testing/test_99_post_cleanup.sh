#! /bin/bash
#
#	test_post_cleanup.sh
#

if [ -d "${DATASET}.dmftar" ]
then
	chmod 0700 "${DATASET}.dmftar"
	rm -rf "${DATASET}.dmftar"

	if [ -d "{$DATASET}.dmftar" ]
	then
		echo "fail: directory ${DATASET}.dmftar still exists"
		exit 1
	fi
fi

if [ -d "${DATASET}.out" ]
then
	chmod 0700 "${DATASET}.out"
	rm -rf "${DATASET}.out"

	if [ -d "{$DATASET}.out" ]
	then
		echo "fail: directory ${DATASET}.out still exists"
		exit 1
	fi
fi

# EOB
