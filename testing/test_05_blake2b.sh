#! /bin/bash
#
#	test_blake2b.sh
#

#B2SUM_DIGEST=$(echo "hello, sailor" |b2sum |awk '{ print $1 }')
HEXDIGEST="5c9be0e1949ec0f624a681476e4af9f05f851df7d9e20bbd7afaf96312001866ff939000e1207554e208fb5963e3712add9978b1ec8f98c86222fe753dc3c5d8"

python3 -c "import hashlib; m = hashlib.new('blake2b'); m.update('hello, sailor\n'.encode()); print(m.hexdigest())" >$OUTPUT 2>&1

PYTHON_BLAKE2B=$(cat $OUTPUT)

if [[ $PYTHON_BLAKE2B != $HEXDIGEST ]]
then
	echo "fail: hexdigest mismatch"
	exit 1
fi

exit 0

# EOB
