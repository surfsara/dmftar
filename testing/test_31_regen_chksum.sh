#! /bin/bash
#
#	test_regen_chksum.sh
#

if [ ! -d "${DATASET}.dmftar/0000" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar/0000"
	exit 1
fi

chmod 0700 "${DATASET}.dmftar/0000"
for SUM in "${DATASET}.dmftar/0000/"*.chksum
do
	mv "$SUM" "${SUM}.O"
done

$DMFTAR --regen-checksum -f "${DATASET}.dmftar" >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: there were errors"
	exit 1
fi

# compare with original
for SUM in "${DATASET}.dmftar/0000/"*.chksum
do
	cmp "$SUM" "${SUM}.O"
	if [ $? -ne 0 ]
	then
		echo "fail: difference in checksum: $SUM"
		exit 1
	fi
done

# cleanup checksum files
chmod 0700 "${DATASET}.dmftar/0000/"
rm -f "${DATASET}.dmftar/0000/"*.chksum.O

exit 0

# EOB
