#! /bin/bash
#
#	generate_dataset.sh	WJ118
#	Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

#PATH=/bin:/usr/bin:/sbin:/usr/sbin

LIBDIR=`dirname $0`
if [ -z "$LIBDIR" ]
then
	LIBDIR="."
fi

. $LIBDIR/generate_lib.sh

if [ -z "$GENERATE_LIB_SH" ]
then
	echo
	echo "error: generate_lib.sh not found, aborting"
	echo
	exit -1
fi

# set params

# number of dirs
NUM_TOP_DIRS=1
NUM_SUB_DIRS=2
DIR_DEPTH=4

# number of generated files per type per dir
NUM_LARGE=0
NUM_MEDIUM=6
NUM_SMALL=10

# sizes * GB, MB, KB
LARGE_SIZE=1
MEDIUM_SIZE=16
SMALL_SIZE=256

###

if [ ! -z "$1" ]
then
	BASEDIR="$1"
else
	BASEDIR="."
fi

NUM=1
DESTDIR="$BASEDIR/dataset$NUM"

while [ -e $DESTDIR ]
do
	NUM=$((NUM + 1))
	DESTDIR="$BASEDIR/dataset$NUM"
done
echo "writing $DESTDIR"

mkdir -p $DESTDIR
cd $DESTDIR
if [ $? -ne 0 ]
then
	echo "error: failed to chdir $DESTDIR"
	exit -1
fi

generate_data

# EOB
