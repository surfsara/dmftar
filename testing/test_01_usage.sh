#! /bin/sh
#
#	test_usage.sh
#

# dmftar is run with --debug, but for this test we really don't want debug
# because we test the behaviour when no options are given at all
# so remove option --debug
# NOTE: if the alternate $DMFTAR script passes any extra options, test_01_usage will always fail
# (this is normal and expected)
CMD_DMFTAR="${DMFTAR% --debug}"

$CMD_DMFTAR >$OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: invalid exit status"
	exit 1
fi

LINES=`wc -l $OUTPUT |awk '{ print $1 }'`
if [ $LINES -lt 3 ]
then
	echo "fail: invalid output lines"
	exit 1
fi

HAVE_USAGE=`grep ^usage: $OUTPUT`
if [ -z "$HAVE_USAGE" ]
then
	echo "fail: missing usage line"
	exit 1
fi

HAVE_PROGNAME=`grep dmftar $OUTPUT`
if [ -z "$HAVE_PROGNAME" ]
then
	echo "fail: missing progname"
	exit 1
fi

exit 0

# EOB
