#! /bin/bash
#
#	test_create.sh
#

if [ ! -d "$DATASET" ]
then
	echo "fail: dataset $DATASET not found"
	exit 1
fi

$DMFTAR -c -L 200M -f "${DATASET}.dmftar" "$DATASET" >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: dmftar exited with exit value $RET"
	exit 1
fi

if [ ! -d "${DATASET}.dmftar" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar"
	exit 1
fi

if [ ! -d "${DATASET}.dmftar/0000" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar/0000"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: there were errors in the output"
	exit 1
fi

LAST_VOLUME=$(grep volume $OUTPUT |tail -1 |cut -d\# -f2)

if [ $LAST_VOLUME -lt 4 ]
then
	echo "fail: not enough volumes were created?"
	exit 1
fi


test_volume() {
	# $1 is volume number
	# Returns 1 on error

	if [ $1 -le 1 ]
	then
		VOL_EXT=""
	else
		# append volume number
		VOL_EXT="-$1"
	fi

	VOLUME="${DATASET}.dmftar/0000/${DATASET}.dmftar.tar$VOL_EXT"

	if [ ! -f "$VOLUME" ]
	then
		echo "fail: missing volume: $VOLUME"
		return 1
	fi
	if [ ! -f "${VOLUME}.idx" ]
	then
		echo "fail: missing volume index: ${VOLUME}.idx"
		return 1
	fi
	if [ ! -f "${VOLUME}.chksum" ]
	then
		echo "fail: missing volume checksum: ${VOLUME}.chksum"
		return 1
	fi
}

i=1
while [ $i -le $LAST_VOLUME ]
do
	test_volume $i
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	i=$((i + 1))
done


test_volume_size() {
	# $1 is volume number
	# Returns 1 on error

	if [ $1 -le 1 ]
	then
		VOL_EXT=""
	else
		# append volume number
		VOL_EXT="-$1"
	fi

	VOLUME="${DATASET}.dmftar/0000/${DATASET}.dmftar.tar$VOL_EXT"

	VOLUME_SIZE=$($FILESIZE $VOLUME)

	# tar file must be at least 200 MB in size, but not too large
	if [ $VOLUME_SIZE -le 200000000 -o $VOLUME_SIZE -ge 201000000 ]
	then
		echo "fail: invalid volume file size"
		return 1
	fi
}

# test the volume size
# skip the final volume, which may be smaller
i=1
while [ $i -lt $LAST_VOLUME ]
do
	test_volume_size $i
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	i=$((i + 1))
done

# EOB
