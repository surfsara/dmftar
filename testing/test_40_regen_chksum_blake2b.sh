#! /bin/bash
#
#	test_regen_chksum_blake2b.sh
#

if [ ! -d "${DATASET}.dmftar/0000" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar/0000"
	exit 1
fi

chmod 0700 "${DATASET}.dmftar/0000"
for SUM in "${DATASET}.dmftar/0000/"*.chksum
do
	rm -f $SUM
done

$DMFTAR --regen-checksum --checksum blake2b -f "${DATASET}.dmftar" >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

# verify
$DMFTAR --verify -f "${DATASET}.dmftar" >>$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: there were errors"
	exit 1
fi

exit 0

# EOB
