#! /bin/bash
#
#	test_version.sh
#

$DMFTAR --version >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: there were errors"
	exit 1
fi

grep -q "^dmftar version " $OUTPUT
if [ $? -ne 0 ]
then
	echo "fail: version string not found"
	exit 1
fi

grep -q "^Copyright" $OUTPUT
if [ $? -ne 0 ]
then
	echo "fail: copyright string not found"
	exit 1
fi

# EOB
