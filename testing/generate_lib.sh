#
#	generate_lib.sh		WJ118
#	Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

#
#	* functions to make a large data set
#	* import this lib into shell scripts with the dot command
#

GENERATE_LIB_SH=1

# parameters for random generation are listed in the top
# It is possible to redefine these after importing the lib

# maximum dirs; parameters for random()
NUM_TOP_DIRS=10
NUM_SUB_DIRS=5
DIR_DEPTH=3

# max random number of generated files per type per dir
NUM_LARGE=15
NUM_MEDIUM=25
NUM_SMALL=50

# sizes * GB, MB, KB
LARGE_SIZE=64
MEDIUM_SIZE=900
SMALL_SIZE=900

###

# these are constants
KB=1000
MB=$((KB * 1000))
GB=$((MB * 1000))

MiB=$((1024 * 1024))
MiB10=$((10 * MiB))

# base files containing random data
# will be 1 MiB in size
RBLOCK=rblock.dat

# will be 10 MiB in size
RBLOCK10=rblock10.dat


random() {
	# generate random number between 1 and $1 (including $1)
	python3 -c "import random; print(int(random.random() * $1) + 1)"
}


# generating files: append the random block multiple times

gen_large_file() {
	# $1 is directory
	# $2 is file sequence number
	local fname
	local size
	local loops

	fname="$1/large-${2}.dat"
	size=$(random $LARGE_SIZE)
	echo "$fname  $size GB"
	size=$((size * GB))

	# write data
	loops=$(($size / $MiB10))
	while [ $loops -gt 0 ]
	do
		cat $RBLOCK10 >>$fname
		loops=$((loops - 1))
	done

	# write md5sum file
	md5sum $fname >${fname}.md5
}

gen_medium_file() {
	# $1 is directory
	# $2 is file sequence number
	local fname
	local size
	local loops
	local big

	fname="$1/medium-${2}.dat"
	size=$(random $MEDIUM_SIZE)
	echo "$fname  $size MB"
	size=$((size * MB))

	if [ $size -lt $MiB ]
	then
		size=$MiB
	fi

	big=$((250 * $MiB))
	if [ $size -gt $big ]
	then
		# write data in large blocks
		loops=$(($size / $MiB10))
		while [ $loops -gt 0 ]
		do
			cat $RBLOCK10 >>$fname
			loops=$((loops - 1))
		done
	else
		# write data
		loops=$(($size / $MiB))
		while [ $loops -gt 0 ]
		do
			cat $RBLOCK >>$fname
			loops=$((loops - 1))
		done
	fi

	# write md5sum file
	md5sum $fname >${fname}.md5
}

gen_small_file() {
	# $1 is directory
	# $2 is file sequence number
	local fname
	local size

	fname="$1/small-${2}.dat"
	size=$(random $SMALL_SIZE)
	echo "$fname  $size kB"
	size=$((size * KB))

	# write data
	dd if=$RBLOCK of=$fname bs=$size count=1 >/dev/null 2>&1

	# write md5sum file
	md5sum $fname >${fname}.md5
}

gen_files() {
	# $1 is directory where to put files

	local i=0
	while [[ $i < $NUM_SMALL ]]
	do
		gen_small_file $1 $i
		((i++))
	done

	i=0
	while [[ $i < $NUM_MEDIUM ]]
	do
		gen_medium_file $1 $i
		((i++))
	done

	i=0
	while [[ $i < $NUM_LARGE ]]
	do
		gen_large_file $1 $i
		((i++))
	done
}

gen_subdirs() {
	# $1 is directory where to put subdirs
	local dname

	local i=0
	while [[ $i < $NUM_SUB_DIRS ]]
	do
		dname="$1/sub-${DIRLEVEL}.$i"
		mkdir -p $dname
		((DIRLEVEL++))

		# TD print current dir
		echo "$dname/"

		if [[ $DIRLEVEL < $DIR_DEPTH ]]
		then
			gen_subdirs $dname
		fi

		gen_files $dname

		((DIRLEVEL--))
		((i++))
	done
}

gen_topdirs() {
	local dname

	local i=0
	while [[ $i < $NUM_TOP_DIRS ]]
	do
		dname="top-$i"
		mkdir -p $dname
		((DIRLEVEL++))

		# TD print current dir
		echo "$dname/"

		if [[ $DIRLEVEL < $DIR_DEPTH ]]
		then
			gen_subdirs $dname
		fi

		gen_files $dname

		((DIRLEVEL--))
		((i++))
	done
}

gen_rblock() {
	if [[ $(uname) == "Linux" ]]
	then
		BS="1M"
	else
		BS="1m"
	fi
	# regenerate the random block files
	dd if=/dev/urandom of=$RBLOCK bs=$BS count=1 >/dev/null 2>&1
	dd if=/dev/urandom of=$RBLOCK10 bs=$BS count=10 >/dev/null 2>&1
}

make_index() {
	ls -lR top-* >INDEX
}

generate_data() {
	# status var, must start at zero
	DIR_LEVEL=0

	# start main program
	gen_rblock
	gen_topdirs
	make_index
}

# EOB
