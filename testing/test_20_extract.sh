#! /bin/bash
#
#	test_extract.sh
#

mkdir "${DATASET}.out"
if [ $? -ne 0 ]
then
	echo "fail: failed to mkdir ${DATASET}.out"
	exit 1
fi

# NOTE: because of this 'cd' here, $DMFTAR can not be a relative path
# we wish to extract the .dmftar archive in this subdir
cd "${DATASET}.out"
if [ $? -ne 0 ]
then
	echo "fail: failed to chdir ${DATASET}.out"
	exit 1
fi

$DMFTAR -x -f "../${DATASET}.dmftar" >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	echo "fail: exit value $RET"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: there were errors"
	exit 1
fi

# check that files are the same
ORIG_INDEX="$TMPDIR/test_20_extract.INDEX.O"
(cd .. && ls -lR "$DATASET") |egrep -v ^total >$ORIG_INDEX

NEW_INDEX="$TMPDIR/test_20_extract.INDEX.N"
ls -lR "$DATASET" |egrep -v ^total >$NEW_INDEX

cmp $ORIG_INDEX $NEW_INDEX
if [ $? -ne 0 ]
then
	echo "fail: cmp files has difference"
	exit 1
fi

# verify md5sums of individual files
# we use the original .md5 file against newly extracted file
cd "${DATASET}"
if [ $? -ne 0 ]
then
	echo "fail: toplevel $DATASET dir not found"
	exit 1
fi
find "../../${DATASET}/" -name '*.md5' -print |while read CHKSUM
do
	md5sum -c "$CHKSUM" >/dev/null
	if [ $? -ne 0 ]
	then
		echo "fail: checksum mismatch: $CHKSUM"
	fi
done

# EOB
