#
#   test_dmftar.py   WJ119
#   Copyright 2019 SURFsara B.V.
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''dmftar unit tests'''

import unittest
import hashlib
import os

from dmftar import bytesize, checksum

from dmftar.index import IndexEntry, is_gnu_fileparts
from dmftar.lrucache import LRUCache
from dmftar.volume import Volume


class TestBytesize(unittest.TestCase):
    '''test module bytesize'''

    def test_bytesize_multiplier(self):
        '''test bytesize.parse_multiplier(s)'''

        self.assertEqual(bytesize.parse_multiplier('B'), 1)
        self.assertEqual(bytesize.parse_multiplier('kB'), 1000)
        self.assertEqual(bytesize.parse_multiplier('MB'), 10**6)
        self.assertEqual(bytesize.parse_multiplier('GB'), 10**9)
        self.assertEqual(bytesize.parse_multiplier('TB'), 10**12)
        self.assertEqual(bytesize.parse_multiplier('PB'), 10**15)
        self.assertEqual(bytesize.parse_multiplier('EB'), 10**18)
        self.assertEqual(bytesize.parse_multiplier('ZB'), 10**21)
        self.assertEqual(bytesize.parse_multiplier('YB'), 10**24)
        self.assertEqual(bytesize.parse_multiplier('kiB'), 1024)
        self.assertEqual(bytesize.parse_multiplier('MiB'), 1024 * 1024)
        self.assertEqual(bytesize.parse_multiplier('GiB'), 1024 ** 3)
        self.assertEqual(bytesize.parse_multiplier('TiB'), 1024 ** 4)
        # case in-sensitivity
        self.assertEqual(bytesize.parse_multiplier('KB'), 1000)
        self.assertEqual(bytesize.parse_multiplier('KIB'), 1024)
        # even without bytes
        self.assertEqual(bytesize.parse_multiplier('K'), 1000)
        # ValueError exception
        self.assertRaises(ValueError, bytesize.parse_multiplier, 'QB')
        # return type
        self.assertIsInstance(bytesize.parse_multiplier('K'), int)

    def test_bytesize_parse(self):
        '''test method bytesize.parse()'''

        self.assertEqual(bytesize.parse('128 B'), 128)
        self.assertEqual(bytesize.parse('40 kB'), 40_000)
        self.assertEqual(bytesize.parse('4 MiB'), 4 * 1024 * 1024)
        self.assertEqual(bytesize.parse('200M'), 200_000_000)
        self.assertEqual(bytesize.parse('1.10 MB'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10 Mb'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10 mb'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10 mB'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10mB'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10m'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10M'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10Mb'), 1_100_000)
        self.assertEqual(bytesize.parse('1.10MB'), 1_100_000)
        # ValueError exception
        self.assertRaises(ValueError, bytesize.parse, '')
        # bytes can not be a float, but larger values can be
        self.assertRaises(ValueError, bytesize.parse, '1.10')
        self.assertRaises(ValueError, bytesize.parse, '1 2 3')
        self.assertRaises(ValueError, bytesize.parse, '1 MiB kiB')

    def test_sprint(self):
        '''test method bytesize.sprint(s)'''

        # Note: bytesize.sprint() also implicitly tests bytesize.prefix()

        self.assertEqual(bytesize.sprint(1000), '1.0 kB')
        self.assertEqual(bytesize.sprint(1024, factor=1024), '1.0 kiB')
        self.assertRaises(RuntimeError, bytesize.sprint, value=1, factor=256)



class TestChecksum(unittest.TestCase):
    '''test checksum'''

    def test_blake2b_aa_present(self):
        '''only check that blake2b is present on this system'''

        self.assertEqual('blake2b' in checksum.algorithms(), True)

    def test_blake2b_correctness(self):
        '''verify blake2b correctness'''

        # it is not really our job to test this,
        # but let's verify it for good measure

        m = hashlib.new('blake2b')
        s = 'hello, sailor\n'
        m.update(s.encode())
        # note: the literal hex string was obtained from `b2sum`
        self.assertEqual(m.hexdigest(), '5c9be0e1949ec0f624a681476e4af9f05f851df7d9e20bbd7afaf96312001866ff939000e1207554e208fb5963e3712add9978b1ec8f98c86222fe753dc3c5d8')

    def test_md5_aa_present(self):
        '''only check that md5 is present on this system'''

        # although dmftar can work without md5, it is plausible that
        # there will be existing archives stored with md5 checksum

        self.assertEqual('md5' in checksum.algorithms(), True)

    def test_md5_correctness(self):
        '''verify md5 correctness'''

        # it is not really our job to test this,
        # but let's verify it for good measure

        m = hashlib.new('md5')
        s = 'hello, world\n'
        m.update(s.encode())
        # note: the literal hex string was obtained from `openssl md5`
        self.assertEqual(m.hexdigest(), '22c3683b094136c3398391ae71b20f04')



class TestIndexEntry(unittest.TestCase):
    '''test IndexEntry'''

    def test_parse(self):
        '''test index parsing'''

        entry = IndexEntry()
        # directory
        entry.parse('drwxr-xr-x sscpjong/sscpjong 0 2019-01-11 15:49 testdata/', init_mode=True)
        self.assertEqual(entry.imode, 0o755)
        self.assertEqual(entry.isdir(), True)
        self.assertEqual(entry.isfile(), False)

        # regular file
        entry.parse('-rw-r--r-- sscpjong/sscpjong 47993323520 2018-08-23 14:13 testdata/subdir/large-1.dat', init_mode=True)
        self.assertEqual(entry.imode, 0o644)
        self.assertEqual(entry.isdir(), False)
        self.assertEqual(entry.isfile(), True)
        self.assertEqual(entry.size, 47993323520)

        # old-style multi-volume header
        # IndexEntry.parse() does not / should not raise exception on weird filetype bit
        # Note that IndexEntry does not know that this is a multi-volume header;
        # dmftar uses other checks for that purpose
        entry.parse('M--------- 0/0     27196954112 1970-01-01 01:00 testdata/subdir/large-1.dat--Continued at byte 8800659968--', init_mode=True)

        # new-style multi-volume header
        # IndexEntry does not see the difference with a regular file
        entry.parse('-rw-r--r-- root/root 39078822912 1970-01-01 01:00 testdata/subdir/GNUFileParts.81887/large-1.dat.2', init_mode=True)
        self.assertEqual(entry.isfile(), True)
        self.assertEqual(is_gnu_fileparts(entry.path), True)



class TestLRUCache(unittest.TestCase):
    '''test LRUCache'''

    def test_order(self):
        '''test LRUCache order'''

        c = LRUCache()
        c['z'] = 10
        c['y'] = 20
        c['x'] = 30

        # check the order
        n = 0
        for key in ('z', 'y', 'x'):
            n += 10
            item, value = c.contents.popitem(last=False)
            self.assertEqual(item == key, True)
            self.assertEqual(value == n, True)

    def test_order2(self):
        '''test LRUCache order after getitem'''

        c = LRUCache()
        c['z'] = 10
        c['y'] = 20
        c['x'] = 30

        # getitem: item is re-inserted as last
        _ = c['z']
        # get last item
        item, value = c.contents.popitem(last=True)
        self.assertEqual(item == 'z', True)
        self.assertEqual(value == 10, True)



class ArchiveMockup:
    '''fake Archive class'''

    def __init__(self, name: str) -> None:
        '''initialize instance'''

        self.name = name

    def __str__(self) -> str:
        '''Returns path to archive'''

        return os.path.join('archive', self.name)



class TestVolume(unittest.TestCase):
    '''tests class Volume'''

    def test_volume_number(self):
        '''test number on filename'''

        arch = ArchiveMockup('test')
        vol = Volume(arch)
        # first volume has number one
        self.assertEqual(vol.number, 1)
        # volume as string is the filename
        # volumes are placed under numbered subdir
        # first volume has no number in the filename
        self.assertEqual(str(vol), 'archive/test/0000/test.tar')
        # other volumes have number in the filename
        vol.number = 2
        self.assertEqual(str(vol), f'archive/test/0000/test.tar-{vol.number}')
        # volume subdirs are numbered by the 1000
        vol.number = 999
        self.assertEqual(str(vol), f'archive/test/0000/test.tar-{vol.number}')
        vol.number = 999
        self.assertEqual(str(vol), f'archive/test/0000/test.tar-{vol.number}')
        vol.number = 1000
        self.assertEqual(str(vol), f'archive/test/0001/test.tar-{vol.number}')
        vol.number = 1001
        self.assertEqual(str(vol), f'archive/test/0001/test.tar-{vol.number}')
        vol.number = 1999
        self.assertEqual(str(vol), f'archive/test/0001/test.tar-{vol.number}')
        vol.number = 2000
        self.assertEqual(str(vol), f'archive/test/0002/test.tar-{vol.number}')
        vol.number = 2001
        self.assertEqual(str(vol), f'archive/test/0002/test.tar-{vol.number}')
        vol.number = 2999
        self.assertEqual(str(vol), f'archive/test/0002/test.tar-{vol.number}')
        vol.number = 20_000
        self.assertEqual(str(vol), f'archive/test/0020/test.tar-{vol.number}')
        vol.number = 2_000_000
        self.assertEqual(str(vol), f'archive/test/2000/test.tar-{vol.number}')
        vol.number = 20_000_000
        self.assertEqual(str(vol), f'archive/test/20000/test.tar-{vol.number}')



if __name__ == '__main__':
    unittest.main()

# EOB
