#! /bin/bash

if [ ! -f src/dmftar/main.py ]
then
	echo "error: please chdir to the dmftar/ toplevel source directory"
	exit 1
fi

MYPYPATH=$(pwd)/stubs
export MYPYPATH
PYTHONPATH=$(pwd)/src/dmftar
export PYTHONPATH

if [ ! -z "$@" ]
then
	set -e
	flake8 "$@"
	#mypy --ignore-missing-imports "$@"
	mypy "$@"
	pylint --rcfile .pylintrc "$@"
	vulture "$@"
	exit 0
fi

if [ -z "$GITLAB_CI" ]
then
	set -x
else
	set -x -e
fi

for f in src/dmftar/*.py
do
	flake8 $f
	mypy $f
	pylint --rcfile .pylintrc $f

	# vulture reports many false positives for modules,
	# so it's commented here
	#vulture $f
done

# EOB
