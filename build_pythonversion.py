#! /usr/bin/env python3

'''this command is used by build_rpm.sh
to determine the required python3 package and command
'''

import sys
if sys.version_info < (3, 6):
    # the default python3 is too old
    # there should be a package available named 'python36'
    # (if not, then you are out of luck)
    print('python36 python3.6')
else:
    print('python3 python3')

# EOB

