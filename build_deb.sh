#! /bin/bash
#
#	build_deb.sh
#

if [[ -z $1 ]]
then
	echo "usage: build_deb.sh <release-tag>"
	exit 1
fi

if [[ ! -e dmftar.1.ronn ]]
then
	echo "error: please chdir to the source directory"
	exit 1
fi

CI_COMMIT_TAG=$1

VERSION=$(echo $CI_COMMIT_TAG | awk -F- '{ print $2 }')
RELEASE=$(echo $CI_COMMIT_TAG | awk -F- '{ if ($3) { print $3 } else { print 1 } }')

# debian build system grabs the version number from the changelog
# update the changelog if this version number is not there
# (which is a hacky fix, but we're bound to forget to update it anyway)
if ! grep -q "${VERSION}-$RELEASE" debian/changelog
then
	dch -v "${VERSION}-$RELEASE" --distribution bullseye --package dmftar "changes unknown; built from commit tag"
fi

fakeroot debian/rules clean
PATH=$(pwd)/debian:$PATH fakeroot debian/rules binary
fakeroot debian/rules clean

# EOB
