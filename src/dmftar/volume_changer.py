#
#   dmftar volume_changer.py    WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''dmftar-volume-changer program for 'dmftar' and 'tar --multi-volume' '''

#
#   - volume changer script for 'dmftar.py' and 'tar --multi-volume'
#   - it takes environment variables TAR_xxx as input
#   - writes the name of the next volume to TAR_FD
#   - set TAR_FD=1 for debugging (write to stdout)
#
#   TAR_SUBCOMMAND is -c, -t, -x
#   TAR_ARCHIVE is the filename of the current volume
#   TAR_VOLUME is the number of the next volume
#   TAR_FD is file descriptor to write new name to
#

import sys
import os
import errno
import argparse

from dmftar import stdio
from dmftar.stdio import stdout, error, debug
from dmftar.lib import ExecError, SilentError
from dmftar.archive import Archive
from dmftar.batch import Batch
from dmftar.config import Config
from dmftar.tar_env import TarEnv
from dmftar.checksum import ChecksumError
from dmftar.remote import RemoteError
# used for typing:
from dmftar.volume import Volume

PROG = stdio.PROG = 'dmftar-volume-changer'


def tell_tar(tar_fd: int, vol: Volume) -> None:
    '''tell tar name of next volume
    May raise RuntimeError, OSError
    '''

    if tar_fd < 0:
        # no running tar, probably dmftar is just using us
        # it's OK
        return

    stdout(f'switching to volume #{vol.number}')

    # write name to tar_fd
    # I do it like this because os.write(tar_fd) may return a short write count
    # (which never goes wrong really, but hey)
    tarfile = str(vol)
    try:
        with os.fdopen(tar_fd, 'wb') as f:
            f.write(tarfile.encode())

    except OSError as err:
        raise RuntimeError(f'error on TAR_FD {tar_fd}: {err.strerror}') from err


# main program
# Note: putting it in a separate function scopes the local vars
def change_volume() -> None:
    '''main program for volume-changer
    Tell tar the name of the next volume
    '''

    conf = Config()
    # get parameters from tempfile in environment var
    conf.getenv()

    # get TAR_xxx environment variables
    env = TarEnv()
    env.check()
    env.getenv()

    # make archive object and initialise it from environment vars
    arch = Archive(conf)
    arch.init_from_env(env)

    # if creating an archive ...
    if env.tar_subcommand == '-c':
        # note that the TAR_VOLUME number is always 1 ahead
        vol = arch.volume_bynumber(env.tar_volume - 1)

        # dmget the next-next batch of files
        # DMF is running ahead of tar
        batch = Batch(arch.local_dir(), conf)
        batch.dmget(env.tar_volume + 1)

        # store files
        vol.store()

        # Note: remote store volume will call cleanup_cache()

        # create new volume subdir if we have to
        old_subdir_number = (env.tar_volume - 1) // 1000
        next_subdir_number = env.tar_volume // 1000
        if old_subdir_number != next_subdir_number:
            # we are switching volume subdir
            # lock the previous subdir
            vol.lock_directory()
            # make new subdir
            # note that this potentially leaves behind an empty subdir
            # which needs to be cleaned up by dmftar
            next_vol = arch.volume_bynumber(env.tar_volume)
            assert next_vol is not None     # this helps mypy
            next_vol.mkdir()

    elif env.tar_subcommand in ('-t', '-x'):
        # get volume
        vol = arch.volume_bynumber(env.tar_volume)
        if vol is not None:
            vol.stage()

        # on success, cleanup local cache
        vol = arch.volume_bynumber(env.tar_volume - 1)
        if vol is not None:
            vol.cleanup_cache()

    else:
        raise EnvironmentError(errno.EINVAL, f'invalid TAR_SUBCOMMAND: {arch.tar_subcommand}')

    # tell 'tar' name of next volume
    vol = arch.volume_bynumber(env.tar_volume)
    tell_tar(env.tar_fd, vol)


def get_options() -> None:
    '''parse command-line options'''

    parser = argparse.ArgumentParser(prog=PROG, usage=f'{PROG} [--debug]',
                                     epilog=f'Do not run {PROG} by hand')
    parser.add_argument('--debug', action='store_true', help='Show debug messages')
    opts = parser.parse_args()

    if opts.debug:
        stdio.DEBUG = True


def run() -> None:
    '''volume-changer main program'''

#    debug('start')
    try:
        if not sys.stdout.isatty():
            # turn off debug color codes
            stdio.DEBUG_COLORS = False

        get_options()
        change_volume()

    except (RuntimeError, ValueError, ChecksumError, RemoteError, ExecError) as _err:
        error(str(_err))
        sys.exit(-1)

    except OSError as _err:
        if hasattr(_err, 'filename') and _err.filename is not None:
            error(f'{_err.filename}: {_err.strerror}')
        else:
            error(_err.strerror)
        sys.exit(-1)

    except SilentError:
        sys.exit(-1)

    except KeyboardInterrupt:   # user hit Ctrl-C
        print()
        sys.exit(127)

    debug('volume-changer: end: success')

# EOB
