#
#   dmftar config.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements class Config, that holds config parameters and options'''

import os
import sys
import errno
import tempfile
import shlex
import struct

from typing import List, Union, Optional

from dmftar import bytesize, checksum, lib, remote, stdio
from dmftar.stdio import error, debug


class Config:
    '''represents config parameters and options'''

    DEFAULT_CONFIG = '$PREFIX/etc/dmftar.conf'
    DEFAULT_RC = '.dmftar.conf'

    KB = 1000
    MB = 1000 * KB
    GB = 1000 * MB
    TB = 1000 * GB
    # default volume size
    DEFAULT_VOLUME_SIZE = 100 * GB
    MINIMUM_SIZE = 200 * MB
    MAXIMUM_SIZE = 50 * TB
    DEFAULT_VOLBATCH_SIZE = 2 * TB

    # Note: This must be a GNU tar command
    DEFAULT_TAR_BINARY = '${tar}'
    DEFAULT_VOLUME_CHANGER = '${dmftar-volume-changer}'
    DEFAULT_CMD_RSH = ('${ssh} -o ConnectTimeout=10 '
                       '-o BatchMode=yes -q --')
    DEFAULT_CMD_RCP = ('${scp} -o ConnectTimeout=10 '
                       '-o BatchMode=yes -p $QUIET --')
    DEFAULT_CMD_DMGET = '${dmget} -n -q'
    DEFAULT_CMD_DMPUT = '${dmput} -n'
    DEFAULT_PIPE_CMD_DMGET_BATCH = '${dmget} -0 -n -q'
    DEFAULT_CMD_FIND_BATCH = '${find} $SRCDIR -print0'

    @staticmethod
    def find_config() -> None:
        '''set the default config file'''

        # load system-wide config
        # DEFAULT_CONFIG contains $PREFIX
        # find out what the prefix is and substitute it
        filename = Config.DEFAULT_CONFIG
        prefix = os.path.abspath(os.path.dirname(sys.argv[0]))
        arr = prefix.split(os.sep)
        try:
            # remove last path element (the 'sbin' directory)
            arr.pop()
        except IndexError:
            prefix = ''
        else:
            prefix = os.sep.join(arr)
            if prefix == '/usr':
                prefix = ''

        # it's a little ugly changing a default setting just like that
        # but Python is a dynamic language, so we can
        Config.DEFAULT_CONFIG = filename.replace('$PREFIX', prefix)

    @staticmethod
    def get_default_algorithm() -> str:
        '''Returns default algorithm'''

        available = checksum.algorithms()

        # use blake2b on 64-bit architecture
        # use blake2s on 32-bit architecture
        is_64bit = 8 * struct.calcsize('P') == 64
        if is_64bit and 'blake2b' in available:
            return 'blake2b'
        if 'blake2s' in available:
            return 'blake2s'

        # blake2 is in hashlib.algorithms_guaranteed
        # so we should never get here

        # md5 is in hashlib.algorithms_guaranteed
        # so no worries about it not being available
        return 'md5'

    def __init__(self) -> None:
        '''initialise instance
        May raise ValueError
        '''

        self.verbose = False
        self.quiet = False
        self.debug = False

        self.tar_options = []           # type: List[str]
        self.volume_size = Config.DEFAULT_VOLUME_SIZE
        self.volbatch_size = Config.DEFAULT_VOLBATCH_SIZE
        self.checksumming = True
        self.algorithm = self.get_default_algorithm()

        # these command must be present on PATH
        # May raise ValueError
        self.tar_binary = lib.expand_cmd(Config.DEFAULT_TAR_BINARY)
        self.volume_changer = lib.expand_cmd(Config.DEFAULT_VOLUME_CHANGER)
        self.cmd_find_batch = lib.expand_cmd(Config.DEFAULT_CMD_FIND_BATCH)

        # these are not fatal
        try:
            # cmd_rsh may be missing if not using remote functionality
            self.cmd_rsh = lib.expand_cmd(Config.DEFAULT_CMD_RSH)   # type: Optional[str]
        except ValueError:
            self.cmd_rsh = None

        try:
            # cmd_rcp may be missing if not using remote functionality
            self.cmd_rcp = lib.expand_cmd(Config.DEFAULT_CMD_RCP)   # type: Optional[str]
        except ValueError:
            self.cmd_rcp = None

        try:
            # cmd_dmget may be missing (eg, local workstation)
            self.cmd_dmget = lib.expand_cmd(Config.DEFAULT_CMD_DMGET)   # type: Optional[str]
        except ValueError:
            self.cmd_dmget = None

        try:
            # cmd_dmput may be missing (eg, local workstation)
            self.cmd_dmput = lib.expand_cmd(Config.DEFAULT_CMD_DMPUT)   # type: Optional[str]
        except ValueError:
            self.cmd_dmput = None

        try:
            # may be missing if we don't have dmget
            self.pipe_cmd_dmget_batch = lib.expand_cmd(Config.DEFAULT_PIPE_CMD_DMGET_BATCH)     # type: Optional[str]
        except ValueError as err:
            # only fatal if we do have dmget
            if self.cmd_dmget is not None:
                # re-raise ValueError
                raise err
            #else:
            self.pipe_cmd_dmget_batch = None

        # well ... these are not a truly config variables;
        # just command-line options and temp vars passed via Config object
        # I call them 'parameters';
        # they are not loaded in 'pure' mode, but they are
        # present in the temp file for passing them from the
        # main program to the volume-changer
        self.force_local = False
        self.keep_cache = False
        self.from_file = None       # type: Optional[str]
        self.opt_null = False
        self.regen_index = False
        self.regen_checksum = False
        self.volbatch_start = 1
        self.remote = None          # type: Optional[str]
        self.archive_dir = None     # type: Optional[str]
        self.cache_dir = None       # type: Optional[str]
        self.tmpfile = None         # type: Optional[str]

        if '$PREFIX' in Config.DEFAULT_CONFIG:
            Config.find_config()

    def load(self, filename: Optional[str] = None, hard_fail: bool = False, pure: bool = True) -> None:
        '''load config file
        if not hard_fail then certain exceptions are ignored
        if pure then only 'true' configurables are loaded,
        else it also loads parameters passed between main program
        and the volume-changer

        May raise OSError, RuntimeError
        '''

        if filename is None:
            self.load(Config.DEFAULT_CONFIG)

            # "overload" user config
            try:
                home = os.environ['HOME']
            except KeyError:
                # no HOME dir; ignore loading ~/.configrc
                return

            # load ~/.configrc
            self.load(os.path.join(home, Config.DEFAULT_RC))
            return

        errors = 0
        debug(f'reading config {filename}')
        try:
            with open(filename, encoding='utf-8') as f:
                lineno = 0
                for line in f:
                    lineno += 1
                    line = line.strip()
                    if not line:
                        continue

                    if line[0] == '#':
                        # skip comments
                        continue

                    try:
                        key, value = line.split('=', 1)
                    except ValueError:
                        error(f'{filename}:{lineno}: syntax error')
                        errors += 1
                        continue

                    key = key.strip()
                    if not key:
                        error(f'{filename}:{lineno}: syntax error')
                        errors += 1
                        continue

                    value = value.strip()
                    try:
                        self.set_value(key, value, pure)
                    except ValueError as err:
                        error(f'{filename}:{lineno}: {err}')
                        errors += 1

        except OSError as err:
            if hard_fail:
                # re-raise the OSError
                raise err

            # it is not a hard error
            debug(f'failed to read {filename}: {err.strerror}')
            return

        if errors:
            raise RuntimeError(f'failed to load config {filename}')

    def load_params(self, filename: str) -> None:
        '''load parameters'''

        self.load(filename, hard_fail=True, pure=False)

    def set_value(self, var: str, val: Union[str, bool, List[str]], pure: bool = True) -> None:
        '''set value
        May raise ValueError
        '''

        # just a wrapper around set(), but note
        # the different default in argument 'pure'
        self.set(var, val, pure)

    def set(self, var: str, val: Union[str, bool, List[str]], pure: bool = False) -> None:
        '''set config setting or parameter
        May raise ValueError
        '''

        # note the default pure=False, meaning that
        # we can set parameters that do not belong in config files
        # This is notably used by dmftar_main.get_options()

        if var == 'verbose':
            self.verbose = Config.set_boolean(var, val)
            debug(f'config.verbose = {self.verbose}')

        elif var == 'quiet':
            self.quiet = Config.set_boolean(var, val)
            stdio.QUIET = self.quiet
            debug(f'config.quiet = {self.quiet}')

        elif var == 'debug':
            self.debug = Config.set_boolean(var, val)
            stdio.DEBUG = self.debug
            debug(f'config.debug = {self.debug}')

        elif var == 'tar_binary':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_TAR_BINARY)
                except ValueError as err:
                    raise ValueError(f'invalid tar binary: {val}; {err}') from err

            try:
                self.tar_binary = lib.search_path(val)
            except ValueError as err:
                raise ValueError(f'invalid tar binary: {val}') from err

            debug(f'config.tar_binary = {self.tar_binary}')

        elif var == 'tar_options':
            self.tar_options = Config.set_stringlist(var, val)
            debug(f'config.tar_options = {self.tar_options}')

        elif var == 'volume_size':
            assert isinstance(val, str)
            size = 0
            if not val:
                size = Config.DEFAULT_VOLUME_SIZE
            else:
                try:
                    size = bytesize.parse(val)
                except ValueError as err:
                    raise ValueError(f"invalid volume size: '{val}'") from err

            if size <= 0:
                raise ValueError('invalid volume size')

            if size < Config.MINIMUM_SIZE:
                min_size = bytesize.sprint(Config.MINIMUM_SIZE)
                raise ValueError(f'volume size is too small; minimum is {min_size}')

            if size > Config.MAXIMUM_SIZE:
                max_size = bytesize.sprint(Config.MAXIMUM_SIZE)
                raise ValueError(f'volume size is too large; maximum is {max_size}')

            self.volume_size = size
            debug(f'config.volume_size = {self.volume_size}')

        elif var == 'volbatch_size':
            assert isinstance(val, str)
            size = 0
            if not val:
                size = Config.DEFAULT_VOLBATCH_SIZE
            else:
                try:
                    size = bytesize.parse(val)
                except ValueError as err:
                    raise ValueError(f"invalid volbatch size: '{val}'") from err

            if size <= 0:
                raise ValueError('invalid volbatch size')

            if size < Config.MINIMUM_SIZE:
                min_size = bytesize.sprint(Config.MINIMUM_SIZE)
                raise ValueError(f'volbatch size is too small; minimum is {min_size}')

            if size > Config.MAXIMUM_SIZE:
                max_size = bytesize.sprint(Config.MAXIMUM_SIZE)
                raise ValueError(f'volbatch size is too large; maximum is {max_size}')

            self.volbatch_size = size
            debug(f'config.volbatch_size = {self.volbatch_size}')

        elif var == 'volume_changer':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_VOLUME_CHANGER)
                except ValueError as err:
                    raise ValueError(f'invalid volume changer: {val}; {err}') from err

            try:
                self.volume_changer = lib.search_path(val)
            except ValueError as err:
                raise ValueError(f'invalid volume changer: {val}') from err

            debug(f'config.volume_changer = {self.volume_changer}')

        elif var == 'checksumming':
            self.checksumming = Config.set_boolean(var, val)
            debug(f'config.checksumming = {self.checksumming}')

        elif var == 'algorithm':
            assert isinstance(val, str)
            if not val:
                val = self.get_default_algorithm()

            if val not in checksum.algorithms():
                raise ValueError(f'unsupported checksumming algorithm: {val}')

            self.algorithm = val
            debug(f'config.algorithm = {self.algorithm}')

        elif var == 'cmd_rsh':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_CMD_RSH)
                except ValueError as err:
                    raise ValueError(f'invalid rsh command: {val}; {err}') from err

            assert isinstance(val, str)
            self.cmd_rsh = val
            debug(f'config.cmd_rsh = {self.cmd_rsh}')
            remote.init_cmd_rsh(self.cmd_rsh)

        elif var == 'cmd_rcp':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_CMD_RCP)
                except ValueError as err:
                    raise ValueError(f'invalid rcp command: {val}; {err}') from err

            assert isinstance(val, str)
            remote.CMD_RCP = self.cmd_rcp = val
            debug(f'config.cmd_rcp = {self.cmd_rcp}')
            remote.init_cmd_rcp(self.cmd_rcp)

        elif var == 'cmd_dmget':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_CMD_DMGET)
                except ValueError as err:
                    raise ValueError(f'invalid dmget command: {val}; {err}') from err

            assert isinstance(val, str)
            self.cmd_dmget = val
            debug(f'config.cmd_dmget = {self.cmd_dmget}')

        elif var == 'cmd_dmput':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_CMD_DMPUT)
                except ValueError as err:
                    raise ValueError(f'invalid dmput command: {val}; {err}') from err

            assert isinstance(val, str)
            self.cmd_dmput = val
            debug(f'config.cmd_dmput = {self.cmd_dmput}')

        elif var == 'pipe_cmd_dmget_batch':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_PIPE_CMD_DMGET_BATCH)
                except ValueError as err:
                    raise ValueError(f'invalid pipe command: {val}; {err}') from err

            assert isinstance(val, str)
            self.pipe_cmd_dmget_batch = val
            debug(f'config.pipe_cmd_dmget_batch = {self.pipe_cmd_dmget_batch}')

        elif var == 'cmd_find_batch':
            assert isinstance(val, str)
            if not val:
                try:
                    val = lib.expand_cmd(Config.DEFAULT_CMD_FIND_BATCH)
                except ValueError as err:
                    raise ValueError(f'invalid find command: {val}; {err}') from err

            assert isinstance(val, str)
            self.cmd_find_batch = val
            debug(f'config.cmd_find_batch = {self.cmd_find_batch}')

        elif var == 'force_local' and not pure:
            self.force_local = Config.set_boolean(var, val)
            debug(f'config.force_local = {self.force_local}')

        elif var == 'keep_cache' and not pure:
            self.keep_cache = Config.set_boolean(var, val)
            debug(f'config.keep_cache = {self.keep_cache}')

        # Note: from_file is never passed between main and volume-changer
        # Note: opt_null is never passed between main and volume-changer

        elif var == 'regen_index' and not pure:
            self.regen_index = Config.set_boolean(var, val)
            debug(f'config.regen_index = {self.regen_index}')

        elif var == 'regen_checksum' and not pure:
            self.regen_checksum = Config.set_boolean(var, val)
            debug(f'config.regen_checksum = {self.regen_checksum}')

        elif var == 'volbatch_start' and not pure:
            assert isinstance(val, str)
            try:
                self.volbatch_start = int(val)
            except ValueError as err:
                raise ValueError(f"invalid volbatch_start: '{val}'") from err

            debug(f'config.volbatch_start = {self.volbatch_start}')

        elif var == 'remote' and not pure:
            assert isinstance(val, str)
            if not val:
                self.remote = None
            else:
                self.remote = val
            debug(f'config.remote = {self.remote}')

        elif var == 'archive_dir' and not pure:
            assert isinstance(val, str)
            if not val:
                self.archive_dir = None
            else:
                self.archive_dir = val
            debug(f'config.archive_dir = {self.archive_dir}')

        elif var == 'cache_dir' and not pure:
            assert isinstance(val, str)
            if not val:
                self.cache_dir = None
            else:
                self.cache_dir = val
            debug(f'config.cache_dir = {self.cache_dir}')

        else:
            raise ValueError(f'unknown config parameter: {var}')

    @staticmethod
    def set_boolean(var: str, value: Union[str, bool, List[str]]) -> bool:
        '''interpret string value as boolean parameter
        Returns boolean value
        May raise ValueError
        '''

        if value is None:
            raise ValueError(f'parameter {var} is not set')

        # pylint: disable=I0011,C0123
        if isinstance(value, bool):
            # value already is a boolean
            return value

        assert isinstance(value, str)
        value = value.lower()
        if value in ('1', 'yes', 'on', 'true'):
            return True

        if value in ('0', 'no', 'off', 'false'):
            return False

        raise ValueError(f'invalid boolean value: {var} = {value}')

    @staticmethod
    def set_stringlist(_var: str, value: Union[str, bool, List[str]]) -> List[str]:
        '''interpret string value as list-of-strings value
        Returns list of strings
        May raise ValueError
        '''

        if value is None or not value:
            return []

        if isinstance(value, list):
            # value already is a list (assume: list of strings)
            return value

        assert isinstance(value, str)
        return shlex.split(value.strip())

    @staticmethod
    def error(filename: Optional[str], lineno: int, msg: str) -> None:
        '''print error message with filename + line number'''

        if filename is None:
            stdio.error(msg)
        else:
            stdio.error(f'{filename}:{lineno}: {msg}')

    def getenv(self) -> None:
        '''get config + parameters via tempfile
        temp filename is in environment variable DMFTAR_TMPFILE
        May raise EnvironmentError
        '''

        try:
            filename = os.environ['DMFTAR_TMPFILE']
        except KeyError as err:
            raise EnvironmentError(errno.EINVAL, 'environment variable DMFTAR_TMPFILE is not set') from err

        if not filename:
            raise EnvironmentError(errno.EINVAL, 'environment variable DMFTAR_TMPFILE is not set')

        self.load_params(filename)

        self.tmpfile = filename

    def putenv(self) -> None:
        '''put config settings + params in a temp file
        The temp filename is published in
        environment variable DMFTAR_TMPFILE

        May raise RuntimeError, OSError
        '''

        if self.tmpfile is None:
            # make temp file
            fd, filename = tempfile.mkstemp(suffix='.tmp', prefix='dmftar-')
            if fd < 0 or filename is None:
                raise RuntimeError('failed to create temp file')

            # make file object
            try:
                f = os.fdopen(fd, 'w+b')
            except OSError as err:
                try:
                    os.close(fd)
                except OSError:
                    pass

                try:
                    os.unlink(filename)
                except OSError:
                    pass
                raise RuntimeError(f'failed to make temp file: {filename}: {err.strerror}') from err

        else:
            # we already have a temp file; rewrite it
            filename = self.tmpfile
            f = open(filename, 'w+b')           # type: ignore

        tar_options = lib.shlex_join(self.tar_options)

        remote_addr = self.remote
        if remote_addr is None:
            remote_addr = ''

        archive_dir = self.archive_dir
        if archive_dir is None:
            archive_dir = ''

        cache_dir = self.cache_dir
        if cache_dir is None:
            cache_dir = ''

        debug(f'writing temp file {filename}')
        with f:
            text = (f'''# generated temp file
verbose = {self.verbose}
quiet = {self.quiet}
debug = {self.debug}
tar_binary = {self.tar_binary}
tar_options = {tar_options}
volume_size = {self.volume_size}
volbatch_size = {self.volbatch_size}
volume_changer = {self.volume_changer}
checksumming = {self.checksumming}
algorithm = {self.algorithm}
cmd_rsh = {self.cmd_rsh}
cmd_rcp = {self.cmd_rcp}
force_local = {self.force_local}
keep_cache = {self.keep_cache}
regen_index = {self.regen_index}
regen_checksum = {self.regen_checksum}
volbatch_start = {self.volbatch_start}
remote = {remote_addr}
archive_dir = {archive_dir}
cache_dir = {cache_dir}
''')
            data = text.encode()
            f.write(data)

        os.environ['DMFTAR_TMPFILE'] = filename
        self.tmpfile = filename

    def cleanup(self) -> None:
        '''delete temp file'''

        if self.tmpfile is None:
            return

        debug(f'deleting temp file {self.tmpfile}')
        try:
            os.unlink(self.tmpfile)
        except OSError:
            pass

        self.tmpfile = None

        # remove environment variable
        try:
            del os.environ['DMFTAR_TMPFILE']
        except KeyError:
            pass

# EOB
