#
#   dmftar index.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements IndexEntry
which is used by interactive.py and verify_content.py
'''

import os
import stat
import time

from typing import Dict, Tuple, Optional

from dmftar.stdio import warning


# constant lookup maps for parsing permission bit strings
MODE_MAP_TYPE = {'-': stat.S_IFREG, 'l': stat.S_IFLNK, 'd': stat.S_IFDIR,
                 'c': stat.S_IFCHR, 'b': stat.S_IFBLK, 'p': stat.S_IFIFO,
                 's': stat.S_IFSOCK,
                 # don't know what type it links to ... force IFREG
                 'h': stat.S_IFREG}

MODE_MAP_OWNER = {'-': 0, 'r': 0o400, 'w': 0o200, 'x': 0o100,
                  's': 0o100 | stat.S_ISUID, 'S': stat.S_ISUID}

MODE_MAP_GROUP = {'-': 0, 'r': 0o40, 'w': 0o20, 'x': 0o10,
                  's': 0o10 | stat.S_ISGID, 'S': stat.S_ISGID}

MODE_MAP_OTHER = {'-': 0, 'r': 4, 'w': 2, 'x': 1,
                  't': 1 | stat.S_ISVTX, 'T': stat.S_ISVTX}

# cache maps permission bits string -> mode
MODE_CACHE: Dict[str, int] = {}


class IndexEntry:
    '''represents a single index entry'''

    def __init__(self) -> None:
        '''initialize instance'''

        # fields with defaults just for clarity
        # note that mode, owner, group are string values
        self.mode = '-rw-r--r--'
        self.owner = 'root'
        self.group = 'wheel'
        self.size = 0
        # mtime is a struct_time tuple
        self.mtime = time.gmtime(0)

        # all paths and names appear escaped in the index files
        # call parse(init_unescape=True) to unescape them
        self.path = 'path/name'
        # linkdest is set only for symlinks, hardlinks
        self.linkdest = None                # type: Optional[str]
        # redundant info, but nice to have
        self.name = 'name'

        # numeric mode and filetype
        # these are only valid after parse(init_mode=True)
        self.imode = self.ifmt = 0

    def isdir(self) -> bool:
        '''Returns True if this entry is a directory'''

        return self.mode[0] == 'd'

    def isfile(self) -> bool:
        '''Returns True if this entry is a file'''

        return self.mode[0] == '-'

    def islink(self) -> bool:
        '''Returns True if this entry is a symbolic link'''

        return self.mode[0] == 'l'

    def ishardlink(self) -> bool:
        '''Returns True if this entry is a hard link'''

        return self.mode[0] == 'h'

    def __repr__(self) -> str:
        '''returns string representation
        (used only for debugging)
        '''

        isdir = 'DIR' if self.isdir() else '-'
        return f'<IndexEntry>({self.name},{isdir})'

    def parse(self, line: str, init_unescape: bool = False, init_mode: bool = False) -> None:
        '''fill in stat info from dmftar index line
        May raise ValueError
        '''

        (self.mode, self.owner, self.group, size, mtime, self.path,
         self.linkdest) = self.split_line(line)

        try:
            self.size = int(size)
        except ValueError as err:
            raise ValueError(f'invalid size: {size} {self.path}') from err

        try:
            self.mtime = time.strptime(mtime, '%Y-%m-%d %H:%M')
        except ValueError as err:
            raise ValueError(f'invalid time format: {mtime} {self.path}') from err

        if init_unescape and self.linkdest is not None:
            self.linkdest = unescape(self.linkdest)

        assert self.path
        if init_unescape:
            self.path = unescape(self.path)
        self.name = os.path.basename(self.path)

        if init_mode:
            try:
                mode = self.parse_mode(self.mode)
            except IndexError as err:
                raise ValueError(f"{self.path}: invalid mode '{self.mode}' in index") from err

            self.imode = stat.S_IMODE(mode)
            self.ifmt = stat.S_IFMT(mode)

    @staticmethod
    def split_line(line: str) -> Tuple[str, str, str, str, str, str, Optional[str]]:
        '''splits an index line into fields (all string values)
        Returns tuple: mode, owner, group, size, mtime, path, linkdest
        May raise ValueError
        '''

        # strip trailing newline, but _not_ spaces
        line = line.rstrip('\r\n')

        # An index line consists of the following elements:
        # mode, owner, size, date, mtime, path
        #
        # The owner and path may contain spaces, this requires us to find
        # groups based on the known elements that contain no spaces.
        # This means that a line has the following elements:
        # [mode, owner_0, ..., owner_n, size, date, mtime, path_0, ..., path_m]
        #
        # Since the owner elements and the path elements may contain all numbers.
        # our search strategy is based on the index of the mode, and the date.
        # The data field is always stored in YYYY-MM-DD format.
        linesplit = line.split(" ")
        date_index = 0
        for date_index, field in enumerate(linesplit):
            try:
                time.strptime(field.strip(), '%Y-%m-%d')
                break
            except ValueError:
                pass

        mode_index = 0
        size_index = date_index - 1
        owner_end_index = size_index
        owner_start_index = mode_index + 1
        mtime_index = date_index + 1
        path_start_index = mtime_index + 1

        mode = linesplit[mode_index]
        owner = " ".join(linesplit[owner_start_index:owner_end_index])
        size = linesplit[size_index]
        date = linesplit[date_index]
        mtime = linesplit[mtime_index]
        path = " ".join(linesplit[path_start_index:])

        try:
            owner, group = owner.split('/', 1)
        except ValueError as err:
            raise ValueError(f'parse error for owner: {owner} {path}') from err

        mtime = date + ' ' + mtime

        linkdest = None

        if mode[0] == 'l':
            # it's a symbolic link
            try:
                path, linkdest = path.split(' -> ', 1)
            except ValueError as err:
                raise ValueError(f'parse error for symbolic link: {path}') from err

            if ' -> ' in linkdest:
                warning(f'unable to reliably parse symbolic link: {path} -> {linkdest}')

        elif mode[0] == 'h':
            # it's a hard link
            try:
                path, linkdest = path.split(' link to ', 1)
            except ValueError as err:
                raise ValueError(f'parse error for hard link: {path}') from err

            if ' link to ' in linkdest:
                warning(f'unable to reliably parse hard link: {path} link to {linkdest}')

        # Note: self.linkdest may (or may not) have a trailing slash

        if path[-1] == '/':
            # strip trailing slash
            path = path[:-1]

        return mode, owner, group, size, mtime, path, linkdest

    @staticmethod
    def parse_mode(mode_bits: str) -> int:
        '''Parse the mode_bits string
        Returns the numeric mode
        May raise IndexError on invalid mode bits string
        '''

        if mode_bits in MODE_CACHE:
            return MODE_CACHE[mode_bits]

        mode = 0

        try:
            mode = MODE_MAP_TYPE[mode_bits[0]]
        except KeyError:
            # GNU tar put a weird bit in there;
            # treat as regular file
            mode = stat.S_IFREG

        try:
            mode |= MODE_MAP_OWNER[mode_bits[1]]
            mode |= MODE_MAP_OWNER[mode_bits[2]]
            mode |= MODE_MAP_OWNER[mode_bits[3]]
        except KeyError:
            # GNU tar put a weird bit in there;
            # ignore
            pass

        try:
            mode |= MODE_MAP_GROUP[mode_bits[4]]
            mode |= MODE_MAP_GROUP[mode_bits[5]]
            mode |= MODE_MAP_GROUP[mode_bits[6]]
        except KeyError:
            # GNU tar put a weird bit in there;
            # ignore
            pass

        try:
            mode |= MODE_MAP_OTHER[mode_bits[7]]
            mode |= MODE_MAP_OTHER[mode_bits[8]]
            mode |= MODE_MAP_OTHER[mode_bits[9]]
        except KeyError:
            # GNU tar put a weird bit in there;
            # ignore
            pass

        MODE_CACHE[mode_bits] = mode
        return mode


def unescape(filename: str) -> str:
    '''Filenames in the index are escaped with backslash notation.
    Returns the unescaped filename
    '''

    if '\\' in filename:
        data = filename.encode()
        return data.decode('unicode_escape')

    return filename


def is_gnu_fileparts(filename: str) -> bool:
    '''Returns True if filename looks like a continued multi-volume file
    with special "virtual" subdir 'GNUFileParts.<pid number>'
    '''

    # get the subdir where the file is in
    path, _ = os.path.split(filename)
    if not path:
        return False

    subdir = os.path.basename(path)
    if subdir == 'GNUFileParts':
        return True
    try:
        part, s_num = subdir.split('.')
        if part != 'GNUFileParts':
            return False

        num = int(s_num)
        if num <= 0:
            return False
    except ValueError:
        return False

    # we saw 'GNUFileParts' and we saw dot number, so yes
    return True


# EOB
