#
#   dmftar server.py   WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''dmftar server-side main program handles remote archives
It is a command server that implements things like:

 list archive
 list0 archive
 mkdir archive
 count archive
 dmget volume
 dmput volume
 index volume
 chksum volume
 version
 help

Command arguments are URI encoded
'''

import sys
import os
import argparse
import urllib.parse
import errno
import signal
import stat
import re

from typing import Dict, Tuple, Callable, Any

from dmftar import stdio, lib
from dmftar.stdio import stdout, error, debug
from dmftar.archive import Archive
from dmftar.config import Config
from dmftar.volume import Volume
import dmftar

PROG = stdio.PROG = 'dmftar-server'
SERVER_HELLO = f'{PROG} {dmftar.__version__}'
MAX_LINE = 32 * 1024

# recognize a volume file
REGEX_VOLUME = r'\.tar(-(\d+))?$'


class ServerCommand:
    '''defines server command'''

    def __init__(self, cmd: str, func: Callable[[str], None], help_text: str) -> None:
        '''initialize instance'''

        self.cmd = cmd      # .cmd is unused, actually
        self.func = func
        self.help_text = help_text

    def __str__(self) -> str:
        '''return string object'''

        return self.help_text

    def invoke(self, *args: str) -> None:
        '''execute command handler'''

        self.func(*args)



CMD_TABLE: Dict[str, ServerCommand] = {}
CMD_LIST: Tuple[str, ...] = ()


def init_cmd_table() -> None:
    '''initialize CMD_TABLE'''

    global CMD_TABLE, CMD_LIST

    # PEP-8 is not nice for this kind of tables
    t = {'help':
         ServerCommand('help', cmd_help,
                       'help               Display this information'),
         'list':
         ServerCommand('list', cmd_list,
                       'list archive       List volumes in archive'),
         'list0':
         ServerCommand('list0', cmd_list0,
                       ('list0 archive      '
                        'List volumes in machine-readable format')),
         'exists':
         ServerCommand('exists', cmd_exists,
                       ('exists archive     '
                        'Check if archive directory exists')),
         'count':
         ServerCommand('count', cmd_count,
                       'count archive      Get volume count of archive'),
         'index':
         ServerCommand('index', cmd_index,
                       'index volume       Display volume index file'),
         'chksum':
         ServerCommand('chksum', cmd_chksum,
                       'chksum volume      Display volume checksum file'),
         'size':
         ServerCommand('size', cmd_size,
                       'size volume        Query volume file size'),
         'mkdir':
         ServerCommand('mkdir', cmd_mkdir,
                       'mkdir archive      Make archive directory'),
         'prefetch':
         ServerCommand('prefetch', cmd_prefetch,
                       'prefetch start+num+archive   Pre-stage volumes'),
         'dmget':
         ServerCommand('dmget', cmd_dmget,
                       'dmget volume.tar   Request get volume from tape'),
         'dmput':
         ServerCommand('dmput', cmd_dmput,
                       'dmput volume.tar   Request put volume on tape'),
         'lock':
         ServerCommand('lock', cmd_lock,
                       'lock directory     Set volume directory as read-only'),
         'unlock':
         ServerCommand('unlock', cmd_unlock,
                       ('unlock directory   Set volume directory '
                        'write permission')),
         'delete-archive':
         ServerCommand('delete-archive', cmd_delete_archive,
                       ('delete-archive     Delete archive '
                        '(no questions asked)')),
         'version':
         ServerCommand('version', cmd_version,
                       'version            Display version number'),
         'quit':
         ServerCommand('quit', cmd_quit,
                       'quit               Exit program')}
    CMD_TABLE = t

    # only used to pretty-print help text
    CMD_LIST = ('help', 'list', 'list0', 'exists', 'count', 'index',
                'chksum', 'size', 'mkdir', 'prefetch', 'dmget', 'dmput',
                'lock', 'unlock', 'delete-archive', 'version', 'quit')

    # if this assert fails, there probably is a command missing
    assert len(CMD_TABLE) == len(CMD_LIST)


def _sigalarm_handler(_signum: int, _frame: Any) -> None:
    '''raises OSError on timeout'''

    raise OSError(errno.EINTR, 'timed out')


def server() -> None:
    '''read command and execute it
    May raise OSError, ValueError
    '''

    stdout(SERVER_HELLO)

    errors = 0

    while True:
        sys.stdout.flush()

        # set a timeout for input
        # the timeout will raise OSError and terminate the server
        signal.signal(signal.SIGALRM, _sigalarm_handler)
        signal.alarm(60)

        # read command from stdin
        line = sys.stdin.readline(MAX_LINE)

        # turn off timeout
        signal.alarm(0)

        if not line:
            break

        if len(line) >= MAX_LINE:
            # terminate the server
            raise ValueError('line too long')

        line = line.strip()
        if not line:
            continue

        debug('line == ' + line)

        arr = line.split()
        if len(arr) > 2:
            error('too many arguments')
            continue

        cmd = arr[0]
        if len(arr) > 1:
            # argument (if any) is URI quoted
            arg = urllib.parse.unquote(arr[1])
        else:
            arg = ''

        if not do_command(cmd, arg):
            errors += 1

    sys.stdout.flush()

    if errors:
        # there were errors; messages already printed;
        raise lib.SilentError


def do_command(cmd: str, arg: str) -> bool:
    '''perform server command
    Any (common) exceptions are caught and error message is displayed
    Returns True if there was no error
    '''

    try:
        cmd_table = CMD_TABLE[cmd]
    except KeyError:
        error(f"unknown command '{cmd}'")
    else:
        # if command is known, invoke it
        try:
            cmd_table.invoke(arg)
        except (RuntimeError, ValueError, lib.ExecError) as err:
            error(str(err))
            return False
        except OSError as err:
            if hasattr(err, 'filename') and err.filename is not None:
                error(f'{err.filename}: {err.strerror}')
            else:
                error(err.strerror)
            return False
        except lib.SilentError:
            # messages already printed
            return False

    return True


# command functions

def cmd_help(_arg: str) -> None:
    '''display all commands'''

    for cmd in CMD_LIST:
        stdout(str(CMD_TABLE[cmd]))


def cmd_list(archive: str) -> None:
    '''list volumes in archive'''

    do_list(archive, human_readable=True)


def cmd_list0(archive: str) -> None:
    '''list volumes in archive
    output in machine-readable format: nul terminated strings
    '''

    do_list(archive, human_readable=False)


def do_list(archive: str, human_readable: bool = True) -> None:
    '''list volumes in archive
    in either human_readable format or not;
    machine-readable format is: ascii_size<space>url_quoted_filename<nul>
    May raise OSError, ValueError
    '''

    check_path(archive)

    if not os.path.isdir(archive):
        raise OSError(errno.ENOENT, 'No such directory', archive)

    if not os.path.isdir(os.path.join(archive, '0000')):
        raise ValueError(f'{archive}: not an archive directory')

    sub = 0
    while True:
        archive_subdir = os.path.join(archive, f'{sub:04}')
        if not os.path.isdir(archive_subdir):
            break

        # list every numbered subdir
        try:
            files = os.listdir(archive_subdir)
        except OSError as err:
            raise OSError(err.errno, f'listdir failed: {err.strerror}', archive_subdir) from err

        for filename in files:
            fullpath = os.path.join(archive_subdir, filename)
            try:
                statbuf = os.stat(fullpath)
            except OSError as err:
                raise OSError(err.errno, f'stat failed: {err.strerror}', fullpath) from err

            # print size + filename
            if human_readable:
                stdout(f'{statbuf.st_size:16}  {fullpath}')
            else:
                url_quoted = urllib.parse.quote(fullpath)
                sys.stdout.write(f'{statbuf.st_size} {url_quoted}\x00')

        sub += 1


def cmd_exists(archive: str) -> None:
    '''check whether archive directory exists
    May raise OSError, ValueError
    '''

    check_path(archive)

    try:
        if os.path.exists(archive):
            if not os.path.isdir(archive):
                raise OSError(errno.ENOTDIR, 'Not a directory', archive)

            if not os.path.isdir(os.path.join(archive, '0000')):
                raise ValueError(f'{archive}: not an archive directory')

            # it exists and looks like a valid archive directory
            stdout('OK')
            return
    except OSError as err:
        raise OSError(err.errno, f'stat failed: {err.strerror}', archive) from err

    # a negative ack means that the archive dir does not exist
    # This is not an error; it just doesn't exist
    stdout('NACK')


def cmd_lock(volume_dir: str) -> None:
    '''Locks given directory
    May raise OSError, ValueError
    '''

    archive_dir = archive_path_for_volume_dir(volume_dir)
    check_path(archive_dir)

    try:
        statbuf = os.lstat(volume_dir)
    except OSError as err:
        raise OSError(err.errno, f'stat failed: {err.strerror}', volume_dir) from err

    mode = stat.S_IMODE(statbuf.st_mode)
    if mode & (stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH) != 0:
        # clear write access bits
        mode &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
        try:
            debug(f'chmod {mode:04o} {volume_dir}')
            os.chmod(volume_dir, mode)
        except OSError as err:
            raise OSError(err.errno, f'failed to lock directory: {err.strerror}', volume_dir) from err

    stdout('OK')


def cmd_unlock(volume_dir: str) -> None:
    '''Unlocks given directory'''

    archive_dir = archive_path_for_volume_dir(volume_dir)
    check_path(archive_dir)

    try:
        statbuf = os.lstat(volume_dir)
    except OSError as err:
        raise OSError(err.errno, f'stat failed: {err.strerror}', volume_dir) from err

    mode = stat.S_IMODE(statbuf.st_mode)
    if mode & stat.S_IWUSR == 0:
        # set user write access bit
        mode |= stat.S_IWUSR
        try:
            debug(f'chmod {mode:04o} {volume_dir}')
            os.chmod(volume_dir, mode)
        except OSError as err:
            raise OSError(err.errno, f'failed to unlock directory: {err.strerror}', volume_dir) from err

    stdout('OK')


def cmd_version(_arg: str) -> None:
    '''print version number'''

    stdout(dmftar.__version__)


def cmd_quit(_arg: str) -> None:
    '''exit program'''

    sys.exit(0)


def cmd_prefetch(num_plus_archive: str) -> None:
    '''pre-stage volumes, starting at volume number
    Argument is encoded as:

      start_number "+" num_volumes "+" archive_directory
    '''

    try:
        s_start, s_num, archive = num_plus_archive.split('+', 2)
        start_number = int(s_start)
        if start_number < 1:
            raise ValueError
        num = int(s_num)
        if num <= 0:
            raise ValueError
    except ValueError as err:
        raise ValueError(f'{num_plus_archive}: invalid argument') from err

    check_path(archive)

    if not os.path.isdir(archive):
        raise OSError(errno.ENOENT, 'No such directory', archive)

    if not os.path.isdir(os.path.join(archive, '0000')):
        raise ValueError(f'{archive}: not an archive directory')

    # make archive object
    conf = Config()
    arch = Archive(conf, archive)
    arch.count_volumes(count_local=True)

    if start_number > arch.num_volumes:
        return

    if start_number + num >= arch.num_volumes:
        num = arch.num_volumes - start_number
        if num <= 0:
            return

    volume_batch = [arch.volume_bynumber(x, local=True)
                    for x in range(start_number, start_number + num + 1)]
    arch.dmget_volume_batch(volume_batch)


def cmd_dmget(volume: str) -> None:
    '''issue dmget request to get file from tape'''

    check_path(volume)

    if not os.path.isfile(volume):
        raise OSError(errno.ENOENT, 'No such file', volume)

    if not os.access(volume, os.R_OK):
        raise OSError(errno.EACCES, 'Permission denied', volume)

    path = archive_path_for_volume(volume)

    # make archive object
    conf = Config()
    arch = Archive(conf, path)
    # make volume object
    vol = Volume(arch)
    vol.number = volume_number(volume)

    # dmget the volume
    vol.dmget()


def cmd_dmput(volume: str) -> None:
    '''issue dmput request to put file on tape'''

    check_path(volume)

    if not os.path.isfile(volume):
        raise OSError(errno.ENOENT, 'No such file', volume)

    if not os.access(volume, os.R_OK):
        raise OSError(errno.EACCES, 'Permission denied', volume)

    path = archive_path_for_volume(volume)

    # make archive object
    conf = Config()
    arch = Archive(conf, path)
    # make volume object
    vol = Volume(arch)
    vol.number = volume_number(volume)

    # dmput the volume
    vol.dmput()


def do_dmget_idx(volume: str) -> None:
    '''run dmget index for a given volume filename
    This is used by the 'index' command to prefetch next index file
    '''

    check_path(volume)

    if not os.path.isfile(volume):
        raise OSError(errno.ENOENT, 'No such file', volume)

    path = archive_path_for_volume(volume)

    # make archive object, and count the number of volumes
    conf = Config()
    arch = Archive(conf, path)
    # make volume object
    vol = Volume(arch)
    vol.number = volume_number(volume)

    idx_filename = str(volume) + '.idx'
    if not os.path.isfile(idx_filename):
        raise OSError(errno.ENOENT, 'No such file', idx_filename)

    if not os.access(idx_filename, os.R_OK):
        raise OSError(errno.EACCES, 'Permission denied', volume)

    # dmget the volume index
    vol.dmget_idx()


def cmd_index(volume: str) -> None:
    '''display index file'''

    check_path(volume)

    # issue dmget for next volume.idx file
    volnum = volume_number(volume)
    next_volume = volume_bynumber(volume, volnum + 1)
    if os.path.isfile(next_volume):
        debug(f'dmget next volume index: {next_volume}.idx')
        do_dmget_idx(next_volume)

    lib.cat_file(volume + '.idx')


def cmd_chksum(volume: str) -> None:
    '''display chksum file'''

    check_path(volume)
    lib.cat_file(volume + '.chksum')


def cmd_size(volume: str) -> None:
    '''display size of file'''

    check_path(volume)
    try:
        size = os.path.getsize(volume)
    except OSError as err:
        raise OSError(err.errno, err.strerror, volume) from err

    stdout(f'{size}')


def cmd_count(archive_dir: str) -> None:
    '''display volume count for archive'''

    check_path(archive_dir)

    if not os.path.isdir(archive_dir):
        raise OSError(errno.ENOENT, 'No such directory', archive_dir)

    if not os.path.isdir(os.path.join(archive_dir, '0000')):
        raise ValueError(f'{archive_dir}: not an archive directory')

    # make archive object, and count the number of volumes
    conf = Config()
    arch = Archive(conf, archive_dir)
    arch.count_volumes(count_local=True)
    stdout(f'{arch.num_volumes}')


def cmd_mkdir(archive: str) -> None:
    '''make archive directory'''

    check_path(archive)
    lib.mkdir_p(archive)


def cmd_delete_archive(archive: str) -> None:
    '''delete archive (no questions asked)'''

    check_path(archive)

    if not os.path.isdir(archive):
        raise OSError(errno.ENOENT, 'No such directory', archive)

    if not os.path.isdir(os.path.join(archive, '0000')):
        raise ValueError(f'{archive}: not an archive directory')

    # make archive object
    conf = Config()
    arch = Archive(conf, archive)
    # delete local archive
    arch.delete_local()


def check_path(path: str) -> None:
    '''check path for illegal constructs
    May raise ValueError
    '''

    if not path:
        raise ValueError('no valid path given')

    if path[0] == os.sep:
        raise ValueError('absolute paths are not allowed')

    arr = path.split(os.sep)
    if os.pardir in arr:
        raise ValueError(f"illegal path component '{os.pardir}'")

    if path[0] == '~':
        raise ValueError("invalid character '~' in path")


def archive_path_for_volume(volume: str) -> str:
    '''Returns archive path for a full volume filename
    May raise ValueError
    '''

    arr = volume.split(os.sep)
    if len(arr) < 3:
        raise ValueError(f'invalid volume path: {volume}')

    arr.pop()

    # we don't need the numbered dir path component,
    # but check that it's there
    numbered_dir = arr.pop()
    try:
        _ = int(numbered_dir)
    except ValueError as err:
        # it's not a number
        raise ValueError(f"invalid volume '{volume}' (no numbered dir found)") from err

    path = os.sep.join(arr)
    debug(f'archive path = {path}')
    return path


def archive_path_for_volume_dir(volume_dir: str) -> str:
    '''Returns archive path for a volume directory
    May raise ValueError
    '''

    arr = volume_dir.split(os.sep)
    if len(arr) < 2:
        raise ValueError(f'invalid volume directory: {volume_dir}')

    # we don't need the numbered dir path component,
    # but check that it's there
    numbered_dir = arr.pop()
    try:
        _ = int(numbered_dir)
    except ValueError as err:
        # it's not a number
        raise ValueError(f"invalid volume directory '{volume_dir}' (no numbered dir found)") from err

    path = os.sep.join(arr)
    debug(f'archive path = {path}')
    return path


def volume_bynumber(volume: str, volnum: int) -> str:
    '''Return volume name with seq number #volnum
    Note that the file may not actually exist
    May raise ValueError
    '''

    arr = volume.split(os.sep)
    if len(arr) < 3:
        raise ValueError(f'invalid volume path: {volume}')

    filename = arr.pop()

    # we don't need to have the numbered dir
    # but do check that it's there
    numbered_dir = arr.pop()
    try:
        _ = int(numbered_dir)
    except ValueError as err:
        # it's not a number
        raise ValueError(f"invalid volume '{volume}' (no numbered dir found)") from err

    arr.append(f'{volnum // 1000:04}')

    # note: volume must end with .tar or .tar-#
    try:
        archive, _ = filename.rsplit('.tar')
    except ValueError as err:
        raise ValueError(f'invalid volume name: {volume}') from err

    if volnum <= 1:
        arr.append(archive + '.tar')
    else:
        arr.append(f'{archive}.tar-{volnum}')

    return os.sep.join(arr)


def volume_number(volume: str) -> int:
    '''Return seq number of volume
    May raise ValueError
    '''

    m = re.search(REGEX_VOLUME, volume)
    if not m:
        raise ValueError(f'invalid volume name: {volume}')
    assert m is not None    # this helps mypy

    num_str = m.groups()[1]
    if num_str is None:
        return 1

    try:
        volnum = int(num_str)
    except ValueError as err:
        # shouldn't happen, regex has pattern \d+
        raise ValueError(f'invalid volume number: {volume}') from err

    return volnum


def get_options() -> None:
    '''parse command-line options'''

    parser = argparse.ArgumentParser(prog=PROG, usage=f'{PROG} [--debug]',
                                     epilog=f'Do not run {PROG} by hand')
    parser.add_argument('--debug', action='store_true', help='Show debug messages')
    opts = parser.parse_args()

    if opts.debug:
        stdio.DEBUG = True


def run() -> None:
    '''server main program'''

    try:
        if not sys.stdout.isatty():
            # turn off debug color codes
            stdio.DEBUG_COLORS = False

        get_options()
        _ = Config()        # only to initialize the Config class
        init_cmd_table()
        server()

    except (RuntimeError, ValueError) as _err:
        error(str(_err))
        sys.exit(20)

    except OSError as _err:
        if hasattr(_err, 'filename') and _err.filename is not None:
            error(f'{_err.filename}: {_err.strerror}')
        else:
            error(_err.strerror)
        sys.exit(21)

    except lib.SilentError:
        sys.exit(22)

    except KeyboardInterrupt:   # user hit Ctrl-C
        print()
        sys.exit(127)

# EOB
