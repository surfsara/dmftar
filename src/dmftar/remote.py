#
#   dmftar remote.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''functions that deal with remote invocations'''

import os
import sys
import subprocess
import urllib.parse

from typing import List, Optional

from dmftar import stdio
from dmftar.stdio import debug
from dmftar.lib import ExecError

# Note: these commands must be set via init_cmd_rsh|rcp()
CMD_RSH = None
CMD_RCP = None

CMD_REMOTE_SERVER = 'dmftar-server'
SERVER_IDENT = 'dmftar-server'


class RemoteError(Exception):
    '''errors related to remote functionality'''



def init_cmd_rsh(cmd_rsh: str) -> None:
    '''initialize module global'''

    global CMD_RSH

    CMD_RSH = cmd_rsh


def init_cmd_rcp(cmd_rcp: str) -> None:
    '''initialize module global'''

    global CMD_RCP

    CMD_RCP = cmd_rcp


def command(remote_addr: Optional[str], cmd: str) -> str:
    '''issue a remote command
    The command is handled by dmftar-server
    Returns output
    May raise ExecError, RemoteError, ValueError
    '''

    debug('issue remote cmd: ' + cmd)
    # URI encode the command argument (if any)
    arr = cmd.split(' ', 1)
    if len(arr) > 1:
        input_str = arr[0] + ' ' + urllib.parse.quote(arr[1])
    else:
        input_str = arr[0]

    if remote_addr is None:
        raise ValueError('remote_addr is None')
    assert remote_addr is not None  # this helps mypy

    assert CMD_RSH is not None

    cmd_arr = CMD_RSH.split()
    cmd_arr.append(remote_addr)
    cmd_arr.append(CMD_REMOTE_SERVER)
#    if stdio.DEBUG:
#        cmd_arr.append('--debug')

    debug('running ' + ' '.join(cmd_arr))
    sys.stdout.flush()
    try:
        debug('URI encoded remote cmd = ' + input_str)
        proc = subprocess.run(cmd_arr, shell=False, input=input_str, stdout=subprocess.PIPE,
                              universal_newlines=True, check=True)
        output = proc.stdout
        if not output:
            raise RemoteError('no response from server')

        # server greeted us with a line; strip it
        arr = output.split('\n')

        #if stdio.DEBUG:
        #    # filter out debug messages from server
        #    arr = [x for x in arr if x[:2] != '% ']

        greeting = arr[0]
        if len(arr) == 1:
            output = ''
        else:
            output = '\n'.join(arr[1:])

        # greeting must be "dmftar-server <version>"
        arr = greeting.split(' ', 1)
        if arr[0] != SERVER_IDENT:
            debug('server responded: ' + greeting)
            raise RemoteError('invalid server response')

    except OSError as err:
        raise ExecError(f'{cmd_arr[0]}: {err.strerror}') from err

    except subprocess.CalledProcessError as err:
        # error message will have been printed
        prog = os.path.basename(cmd_arr[0])
        raise RemoteError(f'{prog} exited with return code {proc.returncode}') from err

    return output


def test_rsh(remote_addr: str) -> None:
    '''test if the remote shell is clean
    May raise ExecError, RemoteError
    '''

    if remote_addr is None:
        # we are not going remote
        return

    assert CMD_RSH is not None

    RSH_TEST_MSG = 'remote shell test'
    CMD_RSH_TEST = 'echo ' + RSH_TEST_MSG

    cmd = CMD_RSH + ' ' + remote_addr + ' ' + CMD_RSH_TEST
    debug(f'running {cmd}')
    cmd_arr = cmd.split()
    sys.stdout.flush()
    try:
        proc = subprocess.run(cmd_arr, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, check=True)
        debug(f'test_rsh stdout == {proc.stdout!r}')
        debug(f'test_rsh stderr == {proc.stderr!r}')
        output = proc.stdout.strip()
        if output != RSH_TEST_MSG:
            raise RemoteError('remote shell is not clean')

    except OSError as err:
        raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

    except subprocess.CalledProcessError as err:
        prog = os.path.basename(cmd_arr[0])
        raise RemoteError(f'{prog} exited with return code {proc.returncode}') from err


def copy_stage(remote_addr: str, filelist: List[str], dstdir: str) -> None:
    '''remote-copy filelist from server to local destination directory
    May raise OSError, RemoteError
    '''

    assert CMD_RCP is not None

    # Note: remote dmget is not handled here
    # This func only does the remote-copy action

    if stdio.QUIET:
        opt_q = '-q'
    else:
        opt_q = ''

    # make command array
    cmd_arr = CMD_RCP.replace('$QUIET', opt_q).split()
    for filename in filelist:
        cmd_arr.append(remote_addr + ':' + filename)

    cmd_arr.append(dstdir)      # copy files to dstdir
    debug('running ' + ' '.join(cmd_arr))
    sys.stdout.flush()
    # run remote copy
    ret = subprocess.call(cmd_arr, shell=False)
    if ret != 0:
        # scp will have printed a message about this
        raise RemoteError(f'remote-copy error code: {ret}')


def copy_store(filelist: List[str], remote_addr: str, dstdir: str) -> None:
    '''remote-copy filelist to remote destination directory
    May raise OSError, RemoteError
    '''

    assert CMD_RCP is not None

    # Note: remote dmput is not handled here
    # This func only does the remote-copy action

    if stdio.QUIET:
        opt_q = '-q'
    else:
        opt_q = ''

    # make command array
    cmd_arr = CMD_RCP.replace('$QUIET', opt_q).split()
    cmd_arr.extend(filelist)
    cmd_arr.append(remote_addr + ':' + dstdir)
    debug('running ' + ' '.join(cmd_arr))
    sys.stdout.flush()
    ret = subprocess.call(cmd_arr, shell=False)
    if ret != 0:
        # scp will have printed a message about this
        raise RemoteError(f'remote-copy error code: {ret}')

# EOB
