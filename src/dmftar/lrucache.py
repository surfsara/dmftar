#
#   dmftar lrucache.py  WJ117
#   Copyright 2017 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements an LRU caching class'''

from typing import Any

from collections import OrderedDict


class LRUCache:
    '''an LRU cache storage container'''

    def __init__(self, maxsize: int = 1000) -> None:
        '''initialize instance'''

        self.maxsize = maxsize
        self.contents = OrderedDict()       # type: OrderedDict[Any, Any]

    def __str__(self) -> str:
        '''Returns string value'''

        return str(self.contents)

    def __len__(self) -> int:
        '''Returns number of stored items'''

        return len(self.contents)

    def __getitem__(self, key: str) -> Any:
        '''Returns item by key
        Raises KeyError if no such item
        '''

        if key in self.contents:
            item = self.contents[key]
            del self.contents[key]
            # re-insert item to change order for LRU behavior
            self.contents[key] = item
            return item

        raise KeyError(f'{key!r}')

    def __setitem__(self, key: str, item: Any) -> None:
        '''add item to the cache'''

        if len(self.contents) >= self.maxsize:
            self.expire()

        self.contents[key] = item

    def expire(self) -> None:
        '''expire items from cache'''

        while len(self.contents) >= self.maxsize:
            # pop the first item (FIFO manner)
            _ = self.contents.popitem(last=False)

    def __delitem__(self, key: str) -> None:
        '''delete item from cache by key'''

        del self.contents[key]

    def __contains__(self, key: str) -> bool:
        '''Returns True if key is present'''

        return key in self.contents

# EOB
