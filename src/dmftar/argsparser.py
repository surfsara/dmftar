#
#   dmftar argsparser.py  WJ119
#   Copyright 2019 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements a custom ArgsParser,
which is a subclass of argparse.ArgumentParser
but prints a nice(r) help text
'''

import argparse


class ArgsOption:
    '''represents a single option for the ArgsParser'''

    def __init__(self, *args, metavar='', help_msg=''):
        '''initialize instance'''

        self.options = args
        self.metavar = metavar
        self.help_msg = help_msg

    def __str__(self):
        '''Returns a single line help message'''

        if len(self.options) > 1:
            s_option = ', '.join(self.options)
        else:
            # determine whether self.options[0] is a short or a long option
            if len(self.options[0]) <= 2:
                s_option = self.options[0]
            else:
                # right justified
                s_option = '    ' + self.options[0]

        if self.metavar:
            s_option += '=' + self.metavar

        return f'  {s_option:<24} {self.help_msg}'



class ArgsParser(argparse.ArgumentParser):
    '''custom argument parser based off argparse.ArgumentParser
    which prints a nice(r) help text
    '''

    def __init__(self, **kwargs):
        '''initialize instance'''

        super().__init__(**kwargs)

        self.options = []
        try:
            self.epilog = kwargs['epilog']
        except KeyError:
            self.epilog = ''

    def add_argument(self, *args, **kwargs):
        '''add an option or argument'''

        if not args or (len(args) == 1 and args[0][0] != '-'):
            # not an option
            pass
        else:
            # one or more options in *args
            try:
                help_msg = kwargs['help']
            except KeyError:
                help_msg = ''
            try:
                metavar = kwargs['metavar']
            except KeyError:
                metavar = ''
            option = ArgsOption(*args, metavar=metavar, help_msg=help_msg)
            self.options.append(option)
        # pass it on
        super().add_argument(*args, **kwargs)

    def add_title(self, title: str):
        '''add a special line to the help text'''

        if title:
            title = '\n' + title

        # just add the string to the options list
        # it will be printed by print_help()
        self.options.append(title)

    def print_help(self, _file=None):
        '''print custom help message

        Note: printing to file is not supported
        '''

        self.print_usage()
        print()

        for option in self.options:
            print(str(option))

        if self.epilog:
            print()
            print(self.epilog)

# EOB
