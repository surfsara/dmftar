astroid==2.11.7
flake8==4.0.1
mypy==0.961
mypy-extensions==0.4.3
pyflakes==2.4.0
pylint==2.13.9
pyparsing==3.0.9
vulture==2.5
