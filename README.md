dmftar
======

This software is a tool for archiving large datasets in SURF's
archiving facility. dmftar uses GNU `tar` to pack files together, and
splitting the total dataset into multiple volumes, allowing for easier
transfers.

`dmftar` has builtin checksumming to verify the integrity of the archive.
Separate index files allow for quick searches of specific files.

SURF's long term archive uses HPE DMF (Data Management Framework), and
is backed by tape. `dmftar` detects DMF and takes care of staging files
from tape, if needed.


Copyright and license
---------------------

Copyright 2014 by SURFsara B.V.
Copyright 2021 by SURF bv

You may use the `dmftar` software under terms described in the Apache
License Version 2.0 ("the License").

* http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Installation
------------

Requirements:

* Python >= 3.6 is required
* GNU tar is required

The recommended way to build Python software is to use a virtual environment.
Create and activate a virtual environment:

```
    $ cd $HOME
    $ python3 -m venv dmftar
    $ source dmftar/bin/activate
```

Build requirements:

* git
* python3-venv
* pip3
  - flit
  - blake3
  - xxhash

Install the build requirements from the source dir:

```
    $ pip3 install -U -r requirements.txt
```

Build the software:

```
    $ flit build
```

The output should end with `Built wheel: dist/dmftar-...`.

* recreating the man page `dmftar.1`:

```
    $ ronn --organization SURF -r dmftar.1.ronn
```

Installation:

There are different ways to install Python software depending on your
situation. You may install into a virtual environment, directly onto
the system, or into your user (HOME) directory.

```
    $ flit install
```

Example of a user install using a specific Python interpreter:

```
    $ flit install --user --python /usr/bin/python3.6
```

* if the installation breaks with an ugly stack trace, that is usally a sign
  that the (system) software is too old. Try installation into a new, fresh,
  and updated virtual environment.

* for remote functionality to work correctly, the `dmftar-volume-changer`
  and `dmftar-server` must be available in the user's default PATH.
  Mind that using a virtualenv typically breaks this functionality,
  and you may have to fix this "by hand". The fix is to make these commands
  available in the user's default PATH, so that a command like

    ssh user@remote dmftar-server

  will correctly start the dmftar server-side process.


Running the test-suite
----------------------

Python unittests are run with the command:

```
$ python3 -m unittest test_dmftar
```

The `testing/` directory contains a test-suite for functionality tests.
To be able to run it, you must first set it up.
The `runtests.sh` script finds dmftar (and dmftar-volume-changer) on PATH,
so to test a specific version of dmftar you have to be careful that
PATH points to the correct version.

```
$ SRCDIR=$(pwd)/testing
$ mkdir -p $HOME/dmftar-testing/sw
$ ./setup.py install --prefix=$HOME/dmftar-testing/sw
$ PATH=$HOME/dmftar-testing/sw/bin:$PATH
```

The test-suite runs on a dataset created by `generate_dataset.sh`.
The dataset is randomized, but will be around 900 MB in size.
The dataset will be created in a directory named `dataset1` under
the _current working_ directory.

```
$ cd $HOME/dmftar-testing && $SRCDIR/generate_dataset.sh
```

Multiple datasets may be generated, they will be numbered in sequence.

You should run test suite from within the dir where the dataset dir is.

```
$ cd $HOME/dmftar-testing && $SRCDIR/runtests.sh
```

* set env var `DATASET` to pass an alternate dataset
